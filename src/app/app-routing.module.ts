import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AComponent } from '../app/component/a/a.component';
import { BComponent } from '../app/component/b/b.component';
import { CComponent } from '../app/component/c/c.component';

const routes: Routes = [
  {
    path: 'a',
    pathMatch: 'full',
    component: AComponent,
  },
  {
    path: 'b',
    pathMatch: 'full',
    component: BComponent,
  },
  {
    path: 'c',
    pathMatch: 'full',
    component: CComponent,
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
