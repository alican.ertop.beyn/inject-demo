import { Component, Inject, Injector } from '@angular/core';
import { BaseComponent } from '../base/base.component';

@Component({
  selector: 'app-b',
  templateUrl: './b.component.html',
})
export class BComponent extends BaseComponent {
  constructor(@Inject(Injector) injector: Injector) {
    super(injector);
  }
}
