import { Component, Inject, Injector, OnDestroy, OnInit } from '@angular/core';
import { HttpService } from 'src/app/services/http.service';

@Component({
  selector: 'app-base',
  template: `<div></div>`,
})
export class BaseComponent implements OnInit, OnDestroy {
  constructor(@Inject(Injector) public injector: Injector) {}

  public get httpService(): HttpService {
    return this.injector.get(HttpService);
  }

  ngOnInit(): void {
    this.httpService.logService.testService;
    console.log('init', this.httpService.testing);
  }

  ngOnDestroy(): void {
    console.log('destroy', this.httpService.testing);
  }
}
