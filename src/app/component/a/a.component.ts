import { Component, Inject, Injector } from '@angular/core';
import { BaseComponent } from '../base/base.component';

@Component({
  selector: 'app-a',
  templateUrl: './a.component.html',
})
export class AComponent extends BaseComponent {
  constructor(@Inject(Injector) injector: Injector) {
    super(injector);

    const [times] = this.httpService.testing.split('_');

    this.httpService.testing = `${Number(times ? times : 0) + 1}_const`;
  }
}
