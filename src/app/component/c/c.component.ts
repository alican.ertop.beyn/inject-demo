import { Component, Inject, Injector } from '@angular/core';
import { BaseComponent } from '../base/base.component';

@Component({
  selector: 'app-c',
  templateUrl: './c.component.html',
})
export class CComponent extends BaseComponent {
  constructor(@Inject(Injector) injector: Injector) {
    super(injector);
  }
}
