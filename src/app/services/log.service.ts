import { Inject, Injectable, Injector } from '@angular/core';
import { TestService } from './test.service';

@Injectable({ providedIn: 'root' })
export class LogService {
  constructor(@Inject(Injector) public injector: Injector) {
    console.log('log constructor');
  }

  public get testService(): TestService {
    return this.injector.get(TestService);
  }
}
