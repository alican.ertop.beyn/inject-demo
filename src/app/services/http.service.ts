import { Inject, Injectable, Injector } from '@angular/core';
import { LogService } from './log.service';

@Injectable({ providedIn: 'root' })
export class HttpService {
  constructor(@Inject(Injector) public injector: Injector) {
    console.log('http constructor');
  }

  public get logService(): LogService {
    return this.injector.get(LogService);
  }

  testing = '0_stuff';
}
