import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { AComponent } from './component/a/a.component';
import { BComponent } from './component/b/b.component';
import { CComponent } from './component/c/c.component';

@NgModule({
  imports: [CommonModule, BrowserModule, AppRoutingModule],
  declarations: [AppComponent, AComponent, BComponent, CComponent],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
